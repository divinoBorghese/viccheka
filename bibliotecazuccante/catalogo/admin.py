from django.contrib import admin
admin.autodiscover()
from catalogo.models import *



admin.site.register(Autore)
admin.site.register(Libro)
admin.site.register(Collocazione)
admin.site.register(Stato)
admin.site.register(Sede)
admin.site.register(Editore)
admin.site.register(Prestito)
admin.site.register(Scarico)




