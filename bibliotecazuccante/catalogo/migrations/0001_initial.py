# Generated by Django 3.0 on 2020-04-04 14:48

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Autori',
            fields=[
                ('idAutore', models.AutoField(primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=100)),
                ('cognome', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'autore',
                'verbose_name_plural': 'autori',
                'ordering': ['cognome', 'nome'],
            },
        ),
        migrations.CreateModel(
            name='Autorilibro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idAutore', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Autori')),
            ],
        ),
        migrations.CreateModel(
            name='CodDew',
            fields=[
                ('idDew', models.AutoField(primary_key=True, serialize=False)),
                ('dew', models.CharField(max_length=100, verbose_name='codice dewey')),
                ('argomento', models.CharField(max_length=100, verbose_name='significato')),
            ],
            options={
                'verbose_name': 'classificazione dewey',
                'verbose_name_plural': 'classificazione dewey',
            },
        ),
        migrations.CreateModel(
            name='Collocazione',
            fields=[
                ('idCol', models.AutoField(primary_key=True, serialize=False)),
                ('colloc', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'collocazione',
                'verbose_name_plural': 'possibili collocazioni',
            },
        ),
        migrations.CreateModel(
            name='Editore',
            fields=[
                ('idEditore', models.AutoField(primary_key=True, serialize=False)),
                ('editore', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('idLibro', models.UUIDField(default=uuid.uuid4, help_text='ID unico', primary_key=True, serialize=False)),
                ('dewey', models.CharField(max_length=100, verbose_name='codice Dewey')),
                ('titolo', models.CharField(max_length=100, verbose_name='titolo')),
                ('isbn', models.CharField(help_text='13 cifre <a href="https://www.isbn.it/CODICEISBN.aspx">numero ISBN</a>', max_length=13, verbose_name='ISBN')),
                ('annoPubblicazione', models.DateField(blank=True, null=True)),
                ('prezzo', models.DecimalField(decimal_places=2, max_digits=5)),
                ('pagine', models.IntegerField(blank=True, null=True)),
                ('dataacquisizione', models.DateField()),
                ('descrizione', models.TextField(help_text='Breve descrizione del libro', max_length=1000)),
                ('inprestito', models.BooleanField(default=False)),
                ('Autore', models.ManyToManyField(through='catalogo.Autorilibro', to='catalogo.Autori')),
                ('Editore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalogo.Editore')),
                ('idCollocazione', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Collocazione')),
            ],
            options={
                'verbose_name': 'libro',
                'verbose_name_plural': 'libri',
            },
        ),
        migrations.CreateModel(
            name='Sede',
            fields=[
                ('idSede', models.AutoField(primary_key=True, serialize=False)),
                ('sede', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Stato',
            fields=[
                ('idStato', models.IntegerField(primary_key=True, serialize=False)),
                ('stato', models.CharField(max_length=100, verbose_name='Stato Volume')),
            ],
            options={
                'verbose_name': 'Stato',
                'verbose_name_plural': 'possibili stati',
            },
        ),
        migrations.CreateModel(
            name='Utenti',
            fields=[
                ('idUtente', models.AutoField(primary_key=True, serialize=False)),
                ('CognomeNome', models.CharField(max_length=100)),
                ('classe', models.CharField(max_length=5)),
            ],
        ),
        migrations.CreateModel(
            name='Scarichi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField()),
                ('idLibro', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Libro')),
            ],
        ),
        migrations.CreateModel(
            name='Prestiti',
            fields=[
                ('idPrestito', models.AutoField(primary_key=True, serialize=False)),
                ('dataprelievo', models.DateField()),
                ('datarestituzione', models.DateField()),
                ('idLibro', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Libro')),
                ('idUtente', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Utenti')),
            ],
        ),
        migrations.AddField(
            model_name='libro',
            name='idSede',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Sede'),
        ),
        migrations.AddField(
            model_name='libro',
            name='idStato',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Stato'),
        ),
        migrations.AddField(
            model_name='autorilibro',
            name='idLibro',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Libro'),
        ),
    ]
