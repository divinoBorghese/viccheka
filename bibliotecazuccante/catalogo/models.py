from django.db import models
from django.urls import reverse
import uuid
import os
from django.contrib.auth.models import User


class Autore(models.Model):
    idAutore= models.AutoField(primary_key=True)
    nome = models.CharField(max_length=100)
    cognome = models.CharField(max_length=100)
    class Meta:
        ordering = ['cognome', 'nome']
        verbose_name_plural = 'Autori'
        verbose_name= 'autore'
    def __str__(self):
        return f'{self.cognome}, {self.nome}'

# class Utente(models.Model):
#     idUtente = models.AutoField(primary_key=True)
#     CognomeNome = models.CharField(max_length=100)
#     user = models.OneToOneField(User, on_delete= models.CASCADE)
#     classe = models.CharField(max_length=5)
#     def __str__(self):
#         return self.CognomeNome
#     class Meta:
#         verbose_name_plural = 'Utenti'

class Collocazione(models.Model):
    idCol = models.AutoField(primary_key=True)
    colloc=models.CharField(max_length=100)
    def __str__(self):
        return self.colloc  
    class Meta:
        verbose_name_plural = 'Possibili collocazioni'


class Stato(models.Model):
    idStato = models.IntegerField(primary_key=True)
    stato = models.CharField('Stato Volume',max_length=100)
    def __str__(self):
        return self.stato
    class Meta:
        verbose_name_plural = 'Possibili stati'

class Sede(models.Model):
    idSede = models.IntegerField(primary_key=True)
    sede = models.CharField('Sede',max_length=100, blank = True, null = True)
    class Meta:
        verbose_name_plural = 'Sedi'
    def __str__(self):
        return self.sede


class Editore(models.Model):
    idEditore = models.AutoField(primary_key=True)
    editore = models.CharField(max_length=100)
    class Meta: 
        verbose_name_plural = 'Editori'
    def __str__(self):
        return self.editore

    def get_image_path(instance, filename):
        return os.path.join('photos', str(instance.id), filename)


class Libro(models.Model):
    idLibro = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='ID unico')
    titolo = models.CharField('titolo', max_length=100)
    copertina =  models.ImageField(default='img/default.png', upload_to='img', blank=True, null=True)
    GENERE_CHOICES=(
        ('1','Avventura e azione'),
        ('2', 'Biografia e autobiografia'),
        ('3', 'Romanzo giallo'),
        ('4','Thriller'),
        ('5', 'Romanzo rosa'),
        ('6', 'Fantascienza'),
        ('7','Religione'),
        ('8', 'Psicologia'),
        ('9', 'Inglese'),
    )
    genere = models.CharField(max_length=100, choices=GENERE_CHOICES,null=True)
    isbn = models.CharField('ISBN', max_length=13, help_text='13 cifre <a href="https://www.isbn.it/CODICEISBN.aspx">numero ISBN</a>')
    Editore= models.ForeignKey(Editore, on_delete=models.CASCADE)
    annoPubblicazione = models.DateField(blank=True, null=True)
    prezzo = models.DecimalField(decimal_places=2, max_digits=5)
    pagine = models.IntegerField(null=True, blank= True)
    dataacquisizione = models.DateField()
    descrizione = models.TextField(max_length=1500, help_text='Breve descrizione del libro')
    idCollocazione= models.ForeignKey(Collocazione, on_delete=models.SET_NULL, null=True)
    idSede = models.ForeignKey(Sede, on_delete=models.SET_NULL, null=True)
    inprestito = models.BooleanField(default=False)
    idStato = models.ForeignKey(Stato, on_delete=models.SET_NULL, null=True)   
    def __str__(self):
        return self.titolo + "(" + self.isbn +")"
    def get_genere(self):
        return self.genere
    class Meta:
        verbose_name_plural = 'Libri'

# class (models.Model):
#     idAutoriLibro = models.AutoField(primary_key=True,)
#     idLibro = models.ForeignKey(Libro, on_delete=models.CASCADE, null=True, blank = True)
#     idAutore = models.ForeignKey(Autore, on_delete=models.CASCADE, null=True, blank = True)
#     def _str_(self):
#         return self.idLibro.titolo

#     class Meta:
#         verbose_name_plural = "AutoriLibri"

class Prestito(models.Model):
    idPrestito = models.AutoField(primary_key=True)
    idLibro = models.ForeignKey(Libro, on_delete=models.SET_NULL, null=True)
    nomeUtente= models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    dataprelievo = models.DateField(null= True)
    datarestituzione = models.DateField(null= True)
    def __str__(self):
        return str(self.nomeUtente.username)+ "-> "+ str(self.idLibro.titolo) 
 
    class Meta:
        verbose_name_plural = 'Prestiti'
    

class Scarico(models.Model):
    idLibro = models.ForeignKey(Libro, on_delete=models.SET_NULL, null=True)
    data = models.DateField()
    MOTIVO_CHOICES=(
        ('c','consunto'),
        ('m', 'mancante'),
        ('o', 'obsoleto'),
    )
    motivo = models.CharField(max_length=1, choices=MOTIVO_CHOICES,null=True)
    class Meta:
        verbose_name_plural = 'Scarichi'

    
