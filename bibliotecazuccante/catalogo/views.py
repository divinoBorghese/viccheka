from django.shortcuts import render
from catalogo.models import *
import datetime
# Create your views here.


def index(request):
    return render(request, 'catalogo/index.html', {})
def login(request):
    return render(request, 'catalogo/login.html', {})
def signup(request):
    return render(request, 'catalogo/signup.html', {})    
def myBooks(request):
    queryset = Prestito.objects.filter(nomeUtente=request.user)
    context = {
        "prenotazioni": queryset
    }
    return render(request, 'catalogo/myBooks.html', context )    
def avventura(request):
    queryset =  Libro.objects.filter(genere__in = ('1','Avventura e azione'),)
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def gialli(request):
    queryset =  Libro.objects.filter(genere__in = ('3', 'Romanzo giallo'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def biografia(request):
    queryset =  Libro.objects.filter(genere__in = ('2', 'Biografia e autobiografia'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def thriller(request):
    queryset =  Libro.objects.filter(genere__in = ('4','Thriller'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def rosa(request):
    queryset =  Libro.objects.filter(genere__in = ('5', 'Romanzo rosa'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def fantascienza(request):
    queryset =  Libro.objects.filter(genere__in = ('6', 'Fantascienza'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def religione(request):
    queryset =  Libro.objects.filter(genere__in = ('7','Religione'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def psicologia(request):
    queryset =  Libro.objects.filter(genere__in = ('8', 'Psicologia'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def inglese(request):
    queryset =  Libro.objects.filter(genere__in = ('9', 'Inglese'))
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def all(request):
    queryset =  Libro.objects.all()
    context = {
        "libri": queryset
    }
    return render(request, 'catalogo/avventura.html', context)

def prenotazione(request):
    if request.method == 'POST':
        if request.POST.get('idLibro') and request.POST.get('username'):
            prestito = Prestito()
            libro = Libro.objects.get(idLibro = request.POST.get('idLibro'))
            user = User.objects.get(username=request.user)
            prestito.idLibro = libro
            prestito.nomeUtente = user
            prestito.dataprelievo = datetime.date.today()
            prestito.datarestituzione = datetime.date.today()
            prestito.save()
            libro.inprestito = True
            libro.save()
            return render(request, 'catalogo/avventura.html')
        else:
            return render(request, 'catalogo/avventura.html')
    else:
            return render(request, 'catalogo/avventura.html')
            
def restituzione(request):
    if request.method == 'POST':
        if request.POST.get('idLibro'):
            prestito = Prestito()
            libroPrestito = Prestito.objects.get(idLibro = request.POST.get('idLibro'))
            prestito=libroPrestito
            prestito.datarestituzione = datetime.date.today()
            prestito.idLibro.inprestito = False
            prestito.save()
            libro.save()
            return render(request, 'catalogo/myBooks.html')
        else:
            return render(request, 'catalogo/myBooks.html')
    else:
            return render(request, 'catalogo/myBooks.html')