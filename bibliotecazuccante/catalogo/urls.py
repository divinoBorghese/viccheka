from django.contrib import admin
from django.urls import path
from . import views
from bibliotecazuccante import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from register import views as v
from django.urls import path, include

urlpatterns = [
    path(r'', views.index, name='index'),
    # path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('avventura/', views.avventura, name='avventura'),
    path('biografia/', views.biografia, name='biografia'),
    path('gialli/', views.gialli, name='gialli'),
    path('thriller/', views.thriller, name='thriller'),
    path('rosa/', views.rosa, name='rosa'),
    path('fantascienza/', views.fantascienza, name='fantascienza'),
    path('religione/', views.religione, name='religione'),
    path('psicologia/', views.psicologia, name='psicologia'),
    path('inglese/', views.inglese, name='inglese'),
    path('all/', views.all, name='all'),
    path('myBooks', views.myBooks, name='myBooks'),
    path("register/", v.register, name="register"), 
    path('', include("django.contrib.auth.urls")),
    path('restituzione', views.restituzione, name='restituzione'),
    path('prenotaLibro', views.prenotazione, name='prenotazione'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)